package priv.billingsapp;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.p_v.flexiblecalendar.FlexibleCalendarView;
import com.p_v.flexiblecalendar.view.BaseCellView;
import com.p_v.flexiblecalendar.entity.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  This class is currently Croatian version because it has:
 *  Monday as first day, weekday starting letters set to croatian and croatian text
 *  this class will be changed to support different languages!
 */
public class CalendarActivity extends AppCompatActivity {

    private Map<MyDate,List<CustomEvent>> eventMap;

    CheckBox checkBox, checkBox2;

    TextView monthText;
    public String[] months = {"Siječanj", "Veljača", "Ožujak", "Travanj", "Svibanj", "Lipanj", "Srpanj",
            "Kolovoz", "Rujan", "Listopad", "Studeni", "Prosinac"};

    Calendar c = Calendar.getInstance();
    int year = c.get(Calendar.YEAR);
    int month = c.get(Calendar.MONTH);
    //int day = c.get(Calendar.DAY_OF_MONTH);   NOT USED RIGHT NOW

    public int x = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        setTitle("Tvoj kalendar");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        eventMap = new HashMap<>();

        monthText = (TextView)findViewById(R.id.dateText);
        monthText.setText(months[month] + " " + year + ".");

        checkBox = (CheckBox)findViewById(R.id.checkBox);
        checkBox2 = (CheckBox)findViewById(R.id.checkBox2);

        final FlexibleCalendarView calendarView = (FlexibleCalendarView)findViewById(R.id.calendar_view);
        calendarView.setStartDayOfTheWeek(2);   // setting monday to be the start day of the week
        calendarView.setCalendarView(new FlexibleCalendarView.CalendarView() {
            @Override
            public BaseCellView getCellView(int position, View convertView, ViewGroup parent, int cellType) {
                //customize the date cells
                BaseCellView cellView = (BaseCellView) convertView;
                if (cellView == null) {
                    LayoutInflater inflater = LayoutInflater.from(CalendarActivity.this);
                    cellView = (BaseCellView) inflater.inflate(R.layout.calendar3_date_cell_view, null);
                }
                if (cellType == BaseCellView.TODAY) {
                    cellView.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    cellView.setTextSize(15);
                } else {
                    cellView.setTextColor(getResources().getColor(android.R.color.white));
                    cellView.setTextSize(12);
                }
                return cellView;
            }

            @Override
            public BaseCellView getWeekdayCellView(int position, View convertView, ViewGroup parent) {
                //customize the weekday header cells
                BaseCellView cellView = (BaseCellView) convertView;
                if (cellView == null) {
                    LayoutInflater inflater = LayoutInflater.from(CalendarActivity.this);
                    cellView = (BaseCellView) inflater.inflate(R.layout.calendar3_week_cell_view, null);
                    cellView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    cellView.setTextColor(getResources().getColor(android.R.color.holo_orange_light));
                    cellView.setTextSize(18);
                }
                return cellView;
            }

            @Override
            public String getDayOfWeekDisplayValue(int dayOfWeek, String defaultValue) {
                return String.valueOf(defaultValue.charAt(0));
            }

        });

        // here you can add events for a date
        calendarView.setEventDataProvider(new FlexibleCalendarView.EventDataProvider() {
            @Override
            public List<? extends Event> getEventsForTheDay(int year, int month, int day) {
                MyDate myDate = new MyDate(day,month,year);
                if (eventMap.containsKey(myDate)) {
                    return eventMap.get(myDate);
                }
                return null;
            }
        });

        calendarView.setOnMonthChangeListener(new FlexibleCalendarView.OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month, int direction) {
                changeMonth(year, month);
            }
        });

        calendarView.setOnDateClickListener(new FlexibleCalendarView.OnDateClickListener(){
            @Override
            public void onDateClick(int year,int month, int day) {
                MyDate myDate = new MyDate(day,month,year);
                List<CustomEvent> colorLst1 = new ArrayList<>();
                if(checkBox.isChecked()) {
                    colorLst1.add(new CustomEvent(android.R.color.holo_green_dark));
                    colorLst1.add(new CustomEvent(android.R.color.holo_purple));
                    //colorLst1.add(new CustomEvent(android.R.color.holo_blue_light));
                }
                if(checkBox2.isChecked()){
                    // change color
                }
                eventMap.put(myDate, colorLst1);
                calendarView.refresh();
            }
        });

        Button display = (Button)findViewById(R.id.displayCalButton);
        display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.isShown()) {
                    calendarView.collapse();
                } else {
                    calendarView.expand();
                }
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calendarView.selectDate(c.getTime());
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;

    }

    /**
     * method that changes text in a text box that shows month and year currently displayed
     * @param year currently displayed year
     * @param month currently displayed month
     */
    public void changeMonth(int year, int month){
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, 1);
        monthText.setText(months[month] + " " + year + ".");
    }
}
