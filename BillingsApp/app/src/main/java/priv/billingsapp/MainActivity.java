package priv.billingsapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Calendar App");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button button = (Button)findViewById(R.id.CalendarActivity);
        Button donate = (Button)findViewById(R.id.DonateActivity);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callMethod(CalendarActivity.class);
            }
        });
        donate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showToast("Not implemented yet.");
            }
        });

        FloatingActionButton info = (FloatingActionButton) findViewById(R.id.fab);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callMethod(AboutActivity.class);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            callMethod(SettingsActivity.class);
        }

        if (id == R.id.action_login){
            showToast("Not implemented yet.");
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * method used for disallowing going back through activities when back button is pressed by
     * rather putting an app to the background
     */
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    /**
     * this method starts a new activity with its class
     * @param called reference to a activity's class
     */
    public void callMethod(Class called){
        Intent intent = new Intent(this,called);
        startActivity(intent);
    }

    /**
     * method used for showing toast messages
     * @param text text to be shown in toast message that will be displayed
     */
    public void showToast(String text){
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
    }
}
