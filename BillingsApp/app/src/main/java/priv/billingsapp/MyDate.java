package priv.billingsapp;

/**
 * Created by Bruno on 05/05/2016.
 */
public class MyDate {
    public int date;
    public int month;
    public int year;

    public MyDate(int date, int month, int year) {
        this.date = date;
        this.month = month;
        this.year = year;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + date + month + year;
        return result;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj)
        return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MyDate otherD = (MyDate) obj;
        return (this.date == otherD.date && this.month == otherD.month && this.year == otherD.year);
    }
}
